import pandas as pd
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
import copy
import lemmagen.lemmatizer
from lemmagen.lemmatizer import Lemmatizer
import regex as re
import numpy as np
import time
from datetime import datetime, timedelta
from time import mktime
import dateutil.parser
from sklearn.pipeline import make_pipeline
from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB, BernoulliNB, ComplementNB, MultinomialNB
from sklearn.feature_selection import SelectKBest, chi2
from itertools import combinations_with_replacement, permutations, product
from matplotlib import pyplot
import pickle
import NN_classifier_v2



def read_books():
    bookName = ['ButalskipolicajInCefizelj_ID_8.txt', 'Veveriček posebne sorte Special Squirrel_ID_12.txt', 'Premraženo Sonce Slovene only _ID_15.txt']
    books = []

    for el in bookName:
        tmp = []
        with open(el, 'r', encoding="utf-8-sig") as f:
            for line in f:
                tmpp = line.replace("\n","")
                if tmpp != "":

                    tmp.append(tmpp)
            tmp = tmp[1:]
            if el == 'Veveriček posebne sorte Special Squirrel_ID_12.txt':
                tmp= tmp[1:-2]

            tmp = ' '.join([str(el) for el in tmp])

        books.append(tmp)

    return books

def try_parsing_date(text):
    for fmt in ('%Y-%m-%d %I:%M:%S', '%m/%d/%Y %H:%M'):
        try:
            return time.strptime(text, fmt)
        except ValueError:
            return "nan"

def time_split(parts, sec):

    data=[]
    map=[]

    for el in xls:
        # if [el[0],el[3],el[4]] not in map:
        #     map.append([el[0], el[3],el[4]])
        if [el[0], el[4], 0] not in map:
            map.append([el[0],el[4],0])
            data.append([el])

        else:
            # idx = map.index([el[0],el[3],el[4]])
            idx = map.index([el[0], el[4],0])
            data[idx].append(el)


    for idxd,chat in enumerate(data):
        chat.sort(key=lambda el: el[9])
        deltaTime = timedelta(seconds=sec)
        for idx in range(len(chat[1:])):

            if chat[idx+1][9]-chat[idx][9]>deltaTime:
                data.append(data[idxd][idx+1:])
                data[idxd] = data[idxd][:idx+1]
                map.append(map[idxd])
                map[-1][2]+=1
                break


    for idx,chat in enumerate(data):
        tmp2 = chat[-1][9]-chat[0][9]
        zeroTime = timedelta(seconds=0)
        size = 1.0 / parts

        for message in chat:
            tmp1 = message[9] - chat[0][9]
            message.append(0 if tmp2 == zeroTime else tmp1/tmp2)
            message[16] = int(message[16] / size)

        # print(tmp2)
        #
        # test = [el[16] for el in chat]
        # testClass = list(set(test))
        # testClass.sort()
        # printer = []
        # for testEl in testClass:
        #     printer.append((testEl, test.count(testEl)))
        #
        # # pyplot.plot([x[0] for x in printer], [x[1] for x in printer], 'bo')
        # # pyplot.title(str(map[idx]) + str(len(chat)))
        # # pyplot.show()
        # # print(printer)

def test(i):
    # testing ce bi lahko bili kateri drugi podatki o sporocilu uporabni
    test = [el[i] for el in xls]
    testClass = list(set(test))
    testClass.sort()
    print(len(testClass))
    printer = []
    for testEl in testClass:
        printer.append((testEl, test.count(testEl)))

    print(printer)
    # pyplot.plot([x[0] for x in printer],[x[1] for x in printer])
    # pyplot.show()

def convertTuple(tup):
    str = ''.join(tup)
    return str

def evalate(yp, y, predict):
    print()
    yClass = list(set(y))
    TP = 0
    FP = 0
    FNandTP = 0
    for class_ in yClass:
        TPclass = len([1 for idx in range(len(y)) if y[idx] == class_ and yp[idx] == class_])
        FNandTPclass =  len([1 for idx in range(len(y)) if y[idx] == class_])
        FPclass = len([1 for idx in range(len(y)) if y[idx] != class_ and yp[idx] == class_])
        precision = 0 if TPclass+FPclass == 0 else TPclass/(TPclass+FPclass)
        recall = TPclass/FNandTPclass
        f1 = 0 if precision+recall == 0 else 2*(precision*recall)/(precision+recall)

        if predict == 'podrobno':
            print("Category:" + categories[class_] + ": precision: " + str(precision) + ", recall: " + str(
                recall) + ", f1: " + str(f1))
        elif predict == 'splosno':
            print("Category:" + categoriesBroad[class_] + ": precision: " + str(precision) + ", recall: " + str(
                recall) + ", f1: " + str(f1))
        elif predict == 'knjige':
            print("Category:" + str(categoriesBook[class_]) + ": precision: " + str(precision) + ", recall: " + str(
                recall) + ", f1: " + str(f1))

        TP += TPclass
        FNandTP += FNandTPclass
        FP += FPclass
    precision = TP / (TP + FP)
    recall = TP / FNandTP
    f1 = 2 * (precision * recall) / (precision + recall)
    print("Evaluation all together: precision: " + str(precision) + ", recall: " + str(
        recall) + ", f1: " + str(f1))

def vec_of_product(k):
    aceceda = ['a', 'b', 'c', 'č', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 'š', 't',
               'u', 'v', 'z', 'ž', ' ']

    comb = list(product(aceceda, repeat=k))

    for i in range(len(comb)):
        comb[i] = convertTuple(comb[i])

    return comb

def vec_of_words(data):
    tmpSet = list(set(data))
    tmpSet = [i for i in tmpSet if len(i) < 16]
    tmpSet.sort()
    tmpSet.append("16+") #ni slovenska beseda ce je daljsa od 16 -> beseda je spam

    return tmpSet

def make_X_words(data, books, language):
    idx = 7 if language == 'SLO' else 8

    # xij je st besede i v sporociju j
    vecWords = vec_of_words(data)
    vecBooks = []
    X = []
    for message in xls:

        tmpTab = np.zeros(len(vecWords))

        # SPREMENI ZA ENG !!! 7->8
        for word in message[idx]:
            if len(word) > 16:
                tmpTab[-1] += 1
            elif word in vecWords:
                tmpTab[vecWords.index(word)] += 1
        X.append(tmpTab)

    for book in books:
        tmpTab = np.zeros(len(vecWords))
        for word in book:
            if len(word) > 16:
                tmpTab[-1] += 1
            elif word in vecWords:
                tmpTab[vecWords.index(word)] += 1
        vecBooks.append(tmpTab)


    X = np.array(X)
    return X, len(vecWords), vecBooks

def make_X_kmer_message(k):
    # xij je stevilo n terice i v sporociju j
    X = []
    comb = vec_of_product(k)
    len_ = len(comb) + 1

    for message in xls:
        tmpTab = np.zeros(len_)
        for idx in range(len(message[7][:(len(message[7]) - k)])):
            try:
                tmpTab[comb.index(message[7][idx:idx + k])] += 1
            except ValueError:
                tmpTab[-1] += 1

        X.append(tmpTab)

    X = np.array(X)
    return X, len_

def make_X_kmer_words(k):
    # xij je stevilo n terice i v sporociju j
    X = []
    comb = vec_of_product(k)
    len_ = len(comb) + 1

    for message in xls:
        tmpTab = np.zeros(len_)
        for string_ in message[7]:
            for idx in range(len(string_[:(len(string_) - k)])):
                try:
                    tmpTab[comb.index(string_[idx:idx + k])] += 1
                except ValueError:
                    tmpTab[-1] += 1
        X.append(tmpTab)

    X = np.array(X)
    return X, len_

def append_feature_to_X(X, i):
    # vector za dodatn podatek (sporocila)(dodatntni podatki o sporocilu so slabi)
    vecPodMap = list(set([el[i] for el in xls]))
    n = len(vecPodMap)
    podMat = []
    for el in xls:
        tmp = np.zeros(n)
        tmp[vecPodMap.index(el[i])] = 1
        podMat.append(tmp)
    podMat = np.array(podMat)
    X = np.concatenate((X, podMat), axis=1)
    return X

def set_parameters(language):
    stop = []
    if language=='SLO':
        lemmatizer = Lemmatizer(dictionary=lemmagen.DICTIONARY_SLOVENE)
        tokenizer = nltk.RegexpTokenizer(r"\w+")
        with open("stop_besede.txt", 'r', encoding="utf-8-sig") as f:
            for line in f:
                stop.append(line[:-1])
    else:
        lemmatizer = WordNetLemmatizer()
        tokenizer = nltk.RegexpTokenizer(r"\w+")
        with open("stop_words.txt", 'r', encoding="utf-8-sig") as f:
            for line in f:
                stop = ((line.replace("‘","")).replace("’","")).replace(" ","").split(",")

    return lemmatizer, tokenizer, stop

def book_to_matrix(books):
    tab = []
    for book in books:
        tmp = tokenizer.tokenize(book)
        tabt = []
        for i in range(len(tmp)):
            tmp[i]=tmp[i].lower()
            tmp[i] = re.sub("\d+", "", tmp[i])  # izbrise stevila
            if tmp[i] not in stop and tmp[i] != '':  # odstrani nepomembne besede
                tmp[i] = lemmatizer.lemmatize(tmp[i])  # odstrani skanjatve
                # if el[idx][i] not in stop and el[idx][i] != '':
                data.append(tmp[i])
                tabt.append(tmp[i])
        tab.append(tabt)
        # for i in range(len(tmp)-1,-1,-1):
        #     if tmp[i]=="":
        #         del tmp[i]

    return tab

def massages_to_matrix(language):
    idx = 7 if language=='SLO' else 8

    for el in xls:
        el[idx] = tokenizer.tokenize(el[idx])
        for i in range(len(el[idx])):
            el[idx][i] = el[idx][i].lower()
            el[idx][i] = re.sub("\d+", "", el[idx][i])  # izbrise stevila

            if el[idx][i] not in stop and el[idx][i] != '':  # odstrani nepomembne besede
                # if el[idx][i] not in stop and el[idx][i] != '':
                el[idx][i] = lemmatizer.lemmatize(el[idx][i])  # odstrani skanjatve
                data.append(el[idx][i])

                # if el[15] != 'O' or el[15] != 'I':
                #     data2.append(el[idx][i])
        # for i in range(len(el[idx])-1,-1,-1):
        #     if el[idx][i]=="":
        #         del el[idx][i]

def delete_empty_messages():
    for idx in range(len(xls)-1,-1,-1):
        if len(xls[idx][8])==0:
            del xls[idx]

def guess_the_book(X,vec):

    map = [8,12,15]
    guess = []

    # Xnew = [[] for el in X]
    # vecnew = [[],[],[]]
    #
    # for idx in range(len(vec[0])):
    #     # if vec[0][idx] != 0.0 and vec[1][idx] != 0.0 and vec[2][idx] != 0.0:
    #     #     continue
    #     if vec[0][idx] == 0.0 and vec[1][idx] == 0.0 and vec[2][idx] == 0.0:
    #         continue
    #
    #     for idx2 in range(3):
    #         vecnew[idx2].append(vec[idx2][idx])
    #     for idx2 in range(len(X)):
    #         Xnew[idx2].append(X[idx2][idx])
    #
    # X = Xnew
    # vec=vecnew


    kvadrati = [np.sqrt(sum([np.square(ell) for ell in el])) for el in vec]

    for el in X:
        # kvadrat = np.sqrt(sum([np.square(ell) for ell in el]))
        tmp = [0,0,0]
        # cos podobnost
        for idx in range(len(el)):
            tmp[0] += el[idx]* vec[0][idx]
            tmp[1] += el[idx]* vec[1][idx]
            tmp[2] += el[idx]* vec[2][idx]

        for idx in range(3):
            tmp[idx]=tmp[idx]/kvadrati[idx]

        # manhtn distance NEDELA
        # for idx in range(len(el)):
        #     tmp[0] += np.abs(el[idx]- vec[0][idx])
        #     tmp[1] += np.abs(el[idx]- vec[1][idx])
        #     tmp[2] += np.abs(el[idx]- vec[2][idx])

        # haming distance NEDELA
        # for idx in range(len(el)):
        #     tmp[0] += 1 if (el[idx]==0 and vec[0][idx]!=0) or (el[idx]!=0 and vec[0][idx]==0) else 0
        #     tmp[1] += 1 if (el[idx]==0 and vec[1][idx]!=0) or (el[idx]!=0 and vec[1][idx]==0) else 0
        #     tmp[2] += 1 if (el[idx]==0 and vec[2][idx]!=0) or (el[idx]!=0 and vec[2][idx]==0) else 0


        guess.append(map[np.argmax(tmp)])
    print(guess)

    return guess


books = read_books()

xls= None
data = [] #tu so vsi podatki
# data2 = []#tu so vsi podatki bres O in I iz kategorije 15

# izbrei 'podrobno', 'splosno', 'knjige'
predict = 'splosno'

# izeri jezik SLO/ENG
language = 'SLO'

try:
    # Load data (deserialize)
    with open('xls.pickle', 'rb') as handle:
        xls = pickle.load(handle)
except (OSError, IOError) as e:
    # Store data (serialize)
    xls = pd.read_excel("AllDiscussionDataCODED_USE_THIS_14Feb2020_MH_sprememba.xlsx", nrows=3541, usecols="A:P")
    xls = xls.values.tolist()
    with open('xls.pickle', 'wb') as handle:
        pickle.dump(xls, handle, protocol=pickle.HIGHEST_PROTOCOL)

# vsa sporocila so stringi za lazi preproc (tudi angleska za mozno kasnjeso uporab)
for idx,el in enumerate(xls):
    el[3] = re.findall('[^?^!^.]*[?!.]', el[3])[0].lower()
    el[3] = re.sub(" ", "", el[3])

    if not isinstance(el[7], str):
        el[7] = str(el[7])
    if not isinstance(el[8], str):
        el[8] = str(el[8])

    if str(el[9])[-2:] == 'AM' or str(el[9])[-2:] == 'PM':
        el[9] = str(el[9])[:-3]
    tmp = try_parsing_date(str(el[9]))
    if tmp != "nan":
        el[9] = datetime.fromtimestamp(mktime(tmp))
        # el[9] = datetime.strptime(tmp.strftime('%H:%M:%S'),'%H:%M:%S')
        # el[9] = tmp
    else:
        el[9] = xls[idx-1][9]

# append zaporedna sporocila istega uporabnika iste kategorije
for idx in range(len(xls) - 1, -1, -1):
    # imena os v obliko lazjo za obelavo
    xls[idx][0] = xls[idx][0].lower()
    xls[idx][0] = re.sub(" ", "", xls[idx][0])

    if idx == -1:
        continue
    if xls[idx][6] == xls[idx - 1][6] and xls[idx][14] == xls[idx - 1][14] and xls[idx][15] == xls[idx - 1][15]:
        xls[idx - 1][7] += xls[idx][7]
        xls[idx - 1][8] += xls[idx][8]
        del xls[idx]


lemmatizer, tokenizer, stop = set_parameters(language)
massages_to_matrix(language)

books_new = []
if predict == 'knjige':
    books_new = book_to_matrix(books)

X, dolzina, vec = make_X_words(data,books_new, language)

if predict == 'knjige':
    guess = guess_the_book(X,vec)
    book_y = [el[2] for el in xls]
    map = [8,12,15]
    print("Predict book with book text:")
    for el in map:
        print(el,len([idx for idx in range(len(book_y)) if el == book_y[idx] and el ==guess[idx]]),len([idx for idx in range(len(book_y)) if el == book_y[idx]]))

# k = 3
# X, dolzina = make_X_kmer_message(k)
# X, dolzina = make_X_kmer_words(k)

# primer apenda knjig(2) , user ()
# X = append_feature_to_X(X, 2)
# X = append_feature_to_X(X, 3)
# X = append_feature_to_X(X, 5)

# doda xls 17 stolpec z indxsom dela v katerm se to sporocilo v pogovoru nahaja
# (pogovor je def kot zaporedje sporocil na isti soli v istem book klubu kjer sta dve zaporedni sporocili oddaljeni za najcec sec sekund)
# time_split(parts=5, sec=600)
# test(16)
# X = append_feature_to_X(X, 16)

categories = ["CG", "CB", "CE", "CF", "CO", "CC", "S", "DQ", "DE", "DA", "DAA", "M", "ME", "MQ", "MA", "IQ", "IA",
              "IQA", "II", "O"]
categoriesBroad = ["C", "S", "D", "M", "I", "O"]
categoriesBook = [2, 5, 6, 7, 8, 12, 15]

# izbrei 'podrobno', 'splosno', 'knige'
y = []
for message in xls:
    if predict == 'podrobno': y.append(categories.index(message[14]))
    elif predict == 'splosno':y.append(categoriesBroad.index(message[15]))
    elif predict == 'knjige': y.append(categoriesBook.index(message[2]))

X = np.array(X)
y = np.array(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.5, random_state=102)

#iz tabele vzame le najbolj pomembnih 500 stolpcev
# if dolzina > 1000:
#     skb = SelectKBest(chi2, k=500)
#     X_train = skb.fit_transform(X_train, y_train)
#     X_test = skb.transform(X_test)

gnb = MultinomialNB()
y_pred = gnb.fit(X_train, y_train).predict(X_test)

# gnb1 = BernoulliNB()
# y_pred1 = gnb1.fit(X_train, y_train).predict(X_test)
#
# gnb2 = ComplementNB()
# y_pred2 = gnb2.fit(X_train, y_train).predict(X_test)

clf = LogisticRegression(random_state=0).fit(X_train, y_train)
y_pred3 = clf.predict(X_test)

clf2 = svm.SVC().fit(X_train, y_train)
y_pred4 = clf2.predict(X_test)

evalate(y_pred, y_test, predict)
print("MultinomialNB number of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_pred).sum(), (y_test == y_pred).sum()/X_test.shape[0]))
#
# evalate(y_pred1, y_test, predict)
# print("BernoulliNB number of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_pred1).sum(), (y_test == y_pred1).sum()/X_test.shape[0]))
#
# evalate(y_pred2, y_test, predict)
# print("ComplementNB number of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_pred2).sum(), (y_test == y_pred2).sum()/X_test.shape[0]))

evalate(y_pred3, y_test, predict)
print("Logisticna number of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_pred3).sum(), (y_test == y_pred3).sum()/X_test.shape[0]))

evalate(y_pred4, y_test, predict)
print("SVM number of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_pred4).sum(), (y_test == y_pred4).sum()/X_test.shape[0]))

instancaNN = NN_classifier_v2.NN_classifier(categories)
instancaNN.train_NN(X_train, instancaNN.classifications_to_vetors(y_train), hidden_neurons=80, alpha=0.1, iteracije=200000, dropout=False, dropout_percent=0.2)
with open('instancaNN_2.pickle', 'wb') as handle:
    pickle.dump(instancaNN, handle, protocol=pickle.HIGHEST_PROTOCOL)
# with open('instancaNN.pickle', 'rb') as handle:
# #     instancaNN = pickle.load(handle)

print("Training time was:", instancaNN.used_training_time, "seconds")
y_predNN = instancaNN.classify(X_test,ERROR_THRESHOLD=0.05)
# with open('y_predNN.pickle', 'wb') as handle:
#     pickle.dump(y_predNN, handle, protocol=pickle.HIGHEST_PROTOCOL)
# with open('y_predNN.pickle', 'rb') as handle:
#     y_predNN = pickle.load(handle)


y_predNN_collapse = instancaNN.collapse_clasifications(y_predNN)

print("Neural network of corect points out of a total %d points : %d  (%.3f)" % (X_test.shape[0], (y_test == y_predNN_collapse).sum(), (y_test == y_predNN_collapse).sum()/X_test.shape[0]))