# ONJ text classification

Za zagon so potrbne naslednje knjižnice:
* import pandas as pd

* import nltk

* from nltk.stem import WordNetLemmatizer

* import lemmagen.lemmatizer

* from lemmagen.lemmatizer import Lemmatizer

* import regex as re

* import numpy as np

* import time

* from datetime import datetime, timedelta

* from time import mktime

* from sklearn import svm

* from sklearn.linear_model import LogisticRegression

* from sklearn.model_selection import train_test_split

* from sklearn.naive_bayes import GaussianNB, BernoulliNB, ComplementNB, MultinomialNB

* from itertools import combinations_with_replacement, permutations, product

* import pickle

* import NN_classifier_v2

V datoteki textClassification.py 
 v 390 izberemo katero kategorijo želimo napovedovati 
 v 393 jezik v katerem text naj bo obravnavan 
 in jo nato zaženemo s konzolo
