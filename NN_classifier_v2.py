import json
import datetime
import numpy as np
import time


class NN_classifier:
    """
    NN_classifier takes in "classes list", other parameters are added as they are needed by the functions
    """

    def __init__(self, classes):
        self.classes = classes
        self.synapse_file = 'synapses_ONJ.json'

    # todo add reLU
    def reLU(self, x):
        return

    def sigmoid(self, x):
        x_clipped = np.clip(x, -500, 500) #todo tale del je suspect mislim da unici zadeve
        output = 1 / (1 + np.exp(-x_clipped))
        return output

    def sigmoid_derivative(self, x):
        return (1 - x) * x

    def reconstruct_from_bow(self, s):
        return "not implemented"

    def calculate_output(self, bow_sentence, show_details=False):
        if show_details:
            print("sentence:", self.reconstruct_from_bow(bow_sentence))

        layer_0 = bow_sentence #prvi nivo je BOW
        layer_1 = self.sigmoid(np.dot(layer_0, self.trained_synapse_0)) # input x prvi nivo
        layer_2 = self.sigmoid(np.dot(layer_1, self.trained_synapse_1)) # output
        return layer_2

    def train_NN(self, X_train, y, hidden_neurons=10, alpha=1, iteracije=50000, dropout=False, dropout_percent=0.5):
        start_time = time.time()
        if self.is_NN_trained(X_train, y, hidden_neurons, alpha, iteracije, dropout, dropout_percent):
            return

        print("Training with layer1 w %s neurons, alpha:%s, dropout:%s %s" % (hidden_neurons, str(alpha), dropout, dropout_percent))
        print("In matrix: %sx%s  and Last Layer: %s" % (len(X_train), len(X_train[0]), len(self.classes)))
        np.random.seed(1)

        prev_mean_error = 1
        # randomly initialize our weights with mean 0
        synapse_0 = 2 * np.random.random((len(X_train[0]), hidden_neurons)) - 1
        synapse_1 = 2 * np.random.random((hidden_neurons, len(self.classes))) - 1

        for i in iter(range(iteracije + 1)):
            # poslji naprej skozi layer 0, 1, and 2
            layer0 = X_train
            layer1 = self.sigmoid(np.dot(layer0, synapse_0))

            if (dropout):
                layer1 *= np.random.binomial([np.ones((len(X_train), hidden_neurons))], 1 - dropout_percent)[0] * (1.0 / (1 - dropout_percent))

            layer2 = self.sigmoid(np.dot(layer1, synapse_1))

            layer_2_err = y - layer2 # koliko smo zgrešili #todo razlika kvadratov

            if (i % 10000) == 0:
                if np.mean(np.abs(layer_2_err)) < prev_mean_error:
                    print("Delta po " + str(i) + " iteracijah:" + str(np.mean(np.abs(layer_2_err))))
                    prev_mean_error = np.mean(np.abs(layer_2_err))
                else:
                    print("Prekinitev:", np.mean(np.abs(layer_2_err)), " > ", prev_mean_error)
                    break

            #chain rule
            layer_2_delta = layer_2_err * self.sigmoid_derivative(layer2) #gradient to target, change depending on how sure we are
            layer_1_err = layer_2_delta.dot(synapse_1.T) #how much did l1 contribute to l2 error
            layer_1_delta = layer_1_err * self.sigmoid_derivative(layer1) #gradient to target, change depending on how sure we are

            synapse_0 += alpha * layer0.T.dot(layer_1_delta)  # alpha * weight update
            synapse_1 += alpha * layer1.T.dot(layer_2_delta) #alpha * weight update

        # shrani
        synapse = {'synapse0': synapse_0.tolist(),
                   'synapse1': synapse_1.tolist(),
                   'hidden_neurons': hidden_neurons,
                   'alpha': alpha,
                   'iteracije': iteracije,
                   'data_hash': str(hash(hash(str(X_train)) + hash(str(y)))),
                   'datetime': datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
                   'classes': self.classes
                   }
        synapse_file = "synapses_ONJ.json"

        with open(synapse_file, 'w') as outfile:
            json.dump(synapse, outfile, indent=4, sort_keys=True)
        print("Saved to file:", synapse_file)

        self.used_training_time = time.time() - start_time

    def classify_example(self, sentence, ERROR_THRESHOLD, show_details=False):
        rez = self.calculate_output(sentence, show_details)

        rez = [[i, r] for i, r in enumerate(rez) if r > ERROR_THRESHOLD]
        rez.sort(key=lambda x: x[1], reverse=True)
        return_results = [[self.classes[r[0]], r[1]] for r in rez]
        #print("%s \n classification: %s" % (sentence, return_results))
        return return_results

    def classify(self, X_test, ERROR_THRESHOLD=0.2):
        # load our calculated synapse values
        with open(self.synapse_file) as data_file:
            synapse = json.load(data_file)
            self.trained_synapse_0 = np.asarray(synapse['synapse0'])
            self.trained_synapse_1 = np.asarray(synapse['synapse1'])

        y_rez = []
        for row in X_test:
            y_rez.append(self.classify_example(row, ERROR_THRESHOLD, show_details=False))
        return y_rez

    def classifications_to_vetors(self, classifications):
        """
        turns 3 into [0 0 0 1 0 0 ...]
        """
        rez = []
        for c in classifications:
            vector = []
            for i in range(0, len(self.classes)):
                vector.append(1) if c == i else vector.append(0)
            rez.append(vector)
        return rez

    def collapse_clasifications(self, classifications):
        """
        we return 0 or 1 best classification as the number it originaly was before transforming
        """
        rez = []
        for c in classifications:
            max = 0
            classfy_as = -1
            for i in range(len(c)):
                if c[i][1] > max:
                    max = c[i][1]
                    classfy_as = [self.classes.index(c[i][0])]
            rez.append(classfy_as)
        return rez

    def is_NN_trained(self, X, y, hidden_neurons, alpha, iteracije, dropout, dropout_percent):
        """
        This function checks weather our neural network was already trained with our parameters, otherwise it continues with trainin it
        :return: true or false
        """
        try:
            with open(self.synapse_file) as data_file:
                synapse = json.load(data_file)

                json_data_hash = synapse['data_hash']
                json_hidden_neurons = synapse['hidden_neurons']
                json_alpha = synapse['alpha']
                json_iteracije = synapse['iteracije']

                if json_data_hash == str(hash(hash(str(X)) + hash(str(y)))) and int(json_hidden_neurons) == hidden_neurons and json_alpha == alpha and int(json_iteracije) == iteracije:
                    return True
                else:
                    return False
        except:
            return False
